#!/bin/awk -E
BEGIN {
# ################################################################################################
#
# Internet Archive Classic Search (iacs)
#  Stephen Balbach May 2015
#  iacs2015@nym.hush.com
#  MIT License
#
# Requirement: GNU Awk 3.1.5 (2005) or greater is available for Unix, Windows, Mac, etc
#
#          If running in "standalone" mode on Windows download a GNU Awk binary (awk.exe):
#                 Source 1: (verified works) http://www.klabaster.com/freeware.htm#dl 
#                 Source 2: (unverified) http://sourceforge.net/projects/ezwinports/files/?source=navbar (compiled by a Gawk developer)
#		  Source 3: (won't work, no networking) http://gnuwin32.sourceforge.net/packages/gawk.htm
#                 Source 4: (unverified) https://code.google.com/p/gnu-on-windows/downloads/list
#   ____________________________________________________________________________________________
#
# Configuration variables:
#
#	1. Program filename - the name of the script. 

MyProg = "iacs.awk"

#	2. Run as a "standalone" web server on a PC; or as a "cgi" script on an existing webserver.
#

RunType = "standalone"
# RunType = "cgi"

#	3. If RunType = "standalone" set the following two variables.
#
#          3.1. Port to run the server on. Can be anything. May need to modify your firewall to allow access.
#               Ignore this is if RunType is "cgi".

MyPort = "8080"

#          3.2  Hostname - either a working domain name, a static IP, or if running
#                          on the same computer you're browsing from in "standalone" mode set to "localhost".
#                          Ignore this if RunType is "cgi".

MyHost = "localhost"

#	4. If RunType = "cgi" set the following two variables:
#
#         4.1 URL prefix. Don't include a trailing slash. Ignore if RunType is "standalone"
#

URLPrefix = "http://mydomain.net/cgi-bin"

#	  4.2 Do you have wget on your system? Set to "yes" or "no". If you don't know, set to "?".
#             Setting this will speed up the script so it doesn't have to check if wget exists with each page 
#             load when in "cgi" mode. 
#             If running in "standalone" mode on Windows set to "no" or "?"
#             Wget is an alternative because it handles broken network connections more gracefully than awk
#             but otherwise is not required, though recommended for cgi installs.

# Wget = "yes"
# Wget = "no"
Wget = "?"

#       5. Number of results per page (50 was the IA default). Can by anything based on
#	   your computer speed and memory etc..

Rows = 50

#	6. Page index block size. This is the number of pages displayed in the index bar before 
#          the "Next" button.

PBSize = 20

#
#	7. URL Agent string - this is what Internet Archive sees in their logs. Recommend
#	   setting the name of the program and your contact information, such as an email
#	   or your Internet Archive userid.

Agent = "Internet Archive Classic Search (iacs2015@nym.hush.com)"

#
#       8. To run in "standalone" mode:
#              Start the server: awk -f iacs.awk
#              To access: http://<hostname>:<port>/
#		eg. http://localhost:8080/
#
#
# ##################################################################################################

        main()
}

function main() {

        if ( Wget == "yes" )
            WTA = "wget"
        else if ( Wget == "no" )
            WTA = "awk"
        if ( WTA == "" ) {
            if ( RunType == "standalone" ) printf("%s\n\nSearching for wget.. ", Agent)
            if ( sys2var( sprintf("command -v %s","wget")) ) {
                WTA = "wget"
                if ( RunType == "standalone") printf("Wget found.\n")
            } else {
                WTA = "awk"
                if ( RunType == "standalone") printf("Wget not found. Using awk networking.\n")
            }
        }

	if ( RunType == "cgi" ) {

            print("content=\"text/html; charset=utf-8\>\n")
            print("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n")

            CGI_setup(ENVIRON["REQUEST_METHOD"], ENVIRON["REQUEST_URI"], "")
            GETARG["page"] == "" ? GETARG["page"] = 1 : ""

            if (GETARG["query"] == "" && GETARG["about"] == "" ) {
                printf("%s\n%s\n%s\n", MetaHeader(), PageHeader(), PageFooter())
            } else {
                if ( GETARG["about"] == "1" ) {
                    printf("%s\n%s\n%s\n", MetaHeader(), AboutPage(), PageFooter())
                } else if ( GETARG["query"] != "" ) {
                    Document = search_ia(GETARG["query"], GETARG["page"], GETARG["sort"])  
                    printf("%s\n%s\n%s\n%s\n", MetaHeader(), PageHeader(), Document, PageFooter())
                }
            }
        }

        if ( RunType == "standalone" ) {
	    webserver()
        }
}

# _________________________ web server, static HTML, CGI _______________________________________________________

#
# Web server
#   Credit: adapted from GNU Awk manual
#
function webserver() {

        MyHost == "" ? MyHost = "localhost" : ""
        MyPort == "" ? MyPort = "8080" : ""
        printf("Webserver at http://%s:%s\nOK\n",MyHost,MyPort)

        HttpService = "/inet/tcp/" MyPort "/0/0"
        URLPrefix    = "http://" MyHost ":" MyPort
        while ("scroll" != "codex") {
            RS = ORS = "\r\n"
            Status   = 200          # OK
            Reason   = "OK"
            Header   = MetaHeader()
            Document = PageHeader()
            Footer   = PageFooter()
            if (GETARG["Method"] == "GET") {	 # Entry point to search_ia()
                GETARG["page"] == "" ? GETARG["page"] = 1 : ""
                GETARG["about"] == "1" ? Document = AboutPage() : ""
                GETARG["query"] != "" ? Document = sprintf("%s\n%s\n", PageHeader(), search_ia(GETARG["query"], GETARG["page"], GETARG["sort"])) : ""
            } else if (GETARG["Method"] == "HEAD") {
              # not yet implemented
            } else if (GETARG["Method"] != "") {
                print "bad method", GETARG["Method"]
            }
            Prompt = Header Document Footer
            print "HTTP/1.0", Status, Reason       |& HttpService 
            print "Connection: Close"              |& HttpService
            print "Pragma: no-cache"               |& HttpService
            len = length(Prompt) + length(ORS)
            print "Content-length:", len           |& HttpService
            print ORS Prompt                       |& HttpService
            # ignore all the header lines
            while ((HttpService |& getline) > 0)
                ;
            # stop talking to this client
            close(HttpService)
            # wait for new client request
            HttpService |& getline
            # do some logging
            print systime(), strftime(), $0
            # CGI_setup()
            CGI_setup($1, $2, $3)
        }   
}

#
# Parse CGI environment into global GETARG[] array.
#
function CGI_setup(method, uri, version, 	i) {

        delete GETARG; delete MENU; delete PARAM
        GETARG["Method"] = method
        GETARG["URI"] = uri
        GETARG["Version"] = version
        i = index(GETARG["URI"], "?")
        # is there a "?" indicating a CGI request?

        if (i > 0) {
            split(substr(GETARG["URI"], 1, i-1), MENU, "[/:]")
            split(substr(GETARG["URI"], i+1), PARAM, "&")
            for (i in PARAM) {
                j = index(PARAM[i], "=")
                GETARG[substr(PARAM[i], 1, j-1)] = substr(PARAM[i], j+1)
            }
        } else {    # there is no "?", no need for splitting PARAMs
            split(GETARG["URI"], MENU, "[/:]")
        }
}

#
# Static HTML
#
function MetaHeader(	str) {

        str = "\
            <html><title>Internet Archive Classic Search</title>\
            <head><meta http-equiv=\"Content-Type\"\
            content=\"text/html; charset=utf-8\">\
            </head>\n\
            <body bgcolor=\"#FFFFFF\" text=\"#000000\"\
            link=\"#0000FF\" vlink=\"#0000FF\"\
            alink=\"#0000FF\"> <a name=\"top\">\n\
            <style type=\"text/css\">\
                body {\
                    margin-top:0px;\
                    margin-left:60px;\
                    margin-right:180px;\
                    color:  #000000;\
                    font-size: 10pt;\
                    font-weight: 100;\
                    font-family: Arial, Helvetica, sans-serif;\
                }\
                .centerText{text-align:center;}\
                .centerBigishBold {\
                    text-align:center;\
                    font-size: 12pt;\
                    font-weight: bold;\
                 }\
                .bold {font-weight: 700;}\
                .boldBig {\
                    font-weight: 700;\
                    font-size: 17pt;\
                }\
                .boldSmall {\
                    font-weight: 700;\
                    font-size: 8pt;\
                }\
                .small {font-size: 8pt;}\
                .high {\
                    padding: 1px;\
                    font-weight: 700;\
                    background-color: #CCCCFF;\
                }\
                .tab20  { margin-left: 20px; }\
                .tab80  { margin-left: 80px; }\
                .tab84  { margin-left: 84px; }\
                .tab100 {\
                    margin-left: 100px;\
                    margin-top: 10px;\
                }\
                .tab125 { margin-left: 125px; }\
                .tab160 { margin-left: 160px; }\
                .tab180 { margin-left: 180px; }\
                .tab185 { margin-left: 185px; }\
                .imageicon {\
                    vertical-align:top;\
                }\
            </style>\n"
        return str
}

function PageHeader(      str) {
        str = "\
                <div style = \"text-align:right; \">\
                <a href=\"" URLPrefix "/" MyProg "?about=1" "\" style=\"color: #00C\" >About</a></div>\
                <span class=\"centerText\"><h1>Internet Archive Classic Search</h1>\
                <form method=GET><p>\
                <input type=\"text\" name=query value=\"\" size=60>\
                <br><input type=submit value=\"Search\"></form></p></span>\n"
        return str
}
function PageFooter(	str){
       str = "</body></html>\n"
       return str
}
function AboutPage(     str) {
        str = "\
                <br><br><br><br><br><br><h1>Internet Archive Classic Search</h1>\n\
                IACS emulates the search interface used at the old or \"classic\" IA website.<br>\
                Search results have page numbers, not infinite scroll. Search results are otherwise<br>\
                identical to searching directly on Internet Archive.<br>\n\
                <br>\n\
                The script is GNU awk and will run on any system that has awk including Windows,<br>\
                Unix and Mac. Awk is pre-installed by default on most Unix and Mac. It is freely available for Windows.<br>\
                <br>\n\
                The script runs in two modes:<br>\n\
                <br>\
                1) As a regular CGI script on an existing websever.<br>\
                2) The script includes a built-in webserver and will run on any computer such as Windows.<br>\
                <br>\n\
                To download and instructions: <a href=\"https://bitbucket.org/stbalbach/InternetArchiveClassicSearch\" style=\"color: #00C\" >https://bitbucket.org/stbalbach/InternetArchiveClassicSearch</a><br>\
                Stephen Balbach 2015<br>\
                iacs2015@nym.hush.com<br>\
                <a href=\"http://opensource.org/licenses/MIT\" style=\"color: #00C\" >MIT License</a>"
        return str
}

# _________________________ core function _______________________________________________________

#
# Search Internet Archive and format results.
#
function search_ia(entity, pagenum, sort      ,head,tail,url,xml,c,doc,page,arr,arrg,arrgh,numfound,numpages,myfloat,qin,ipage,zpage,G,i,visible,first,second,str,pageindex,pbon,pbtot,ad,ac) {

        if ( sort == "-publicdate") sort = "publicdate+desc"
        if ( sort == "publicdate")  sort = "publicdate+asc"
        if ( sort == "-date") sort = "date+desc"
        if ( sort == "date")  sort = "date+asc"

        head = "http://archive.org/advancedsearch.php?q="
        tail = "&fl[]=date&fl[]=creator&fl[]=description&fl[]=downloads&fl[]=identifier&fl[]=mediatype&fl[]=subject&fl[]=title&sort[]=" sort "&sort[]=&sort[]=&rows=" Rows "&output=xml&callback=callback&save=yes&page=" pagenum
        url = head entity tail

       # Download XML from Internet Archive
	xml = http2var(url) 
        if ( xml == "" )
          return "Error in function search_ia: Unable to retrieve data from Internet Archive." 
        gsub(/&lt;/,"<",xml);gsub(/&gt;/,">",xml);gsub(/&quot;/,"\"",xml);gsub(/&amp;/,"\\&",xml)

        c = split(xml, doc, "(<doc>|</doc>)")

       # numFound="432"
        match(xml, "numFound=\"[0-9]+\"", arr)
        split(arr[0], arrg, "\"")
        numfound = arrg[2]
        myfloat = numfound / Rows
        if ( myfloat ~ "[.]") {
            split(myfloat, arr, ".")
            numpages = arr[1] + 1
        } else
            numpages = myfloat

     # name="qin">text<    (search string as reported by IA)
        split(doc[1], arr, "(name=\"qin\">)")
        split(arr[2], arrg, "</str>")
        qin = arrg[1]

        numfound > 0 ? first = (pagenum * Rows) - (Rows - 1) : first = 0
        (pagenum * Rows) > numfound ? second = numfound : second = pagenum * Rows

   	init_searchtokens(qin)         # Parse search words for highlighting

        page = page "\n<div class=\"tab80\"><span class=\"boldBig\">Search Results</span></div>\
                     <div class=\"tab84\"><span class=\"boldSmall\">Results:</span><span class=\"small\"> " first " through " second " of </span><span class =\"boldSmall\">" numfound "</span><span class=\"small\"> (" numpages " pages)</span><br>\
                     <span class=\"boldSmall\">You searched for:</span><span class=\"small\"> <a href=\"http://archive.org/search.php?query=" entity "\" style=\"text-decoration: none; color: #00C\" >" qin "</a></span></div>"

        # page index bar
        if ( numpages > 1 ) {
            pageindex = "\n<br><br><div class=\"centerBigishBold\">"
	    pbon = pageblock(pagenum, numpages, "on")
	    pbtot = pageblock(pagenum, numpages, "total")

            pbon == 1 ? ipage = 0 : ipage = ((int(pbon) * int(PBSize)) - (int(PBSize)))
            pbon == pbtot ? zpage = numpages : zpage = ipage + (int(PBSize) )

            # "Prev"
            if ( pbtot > 1 && pbon != 1) {
                if ( RunType == "cgi" )
                    pageindex = pageindex "<a href=\"" URLPrefix "/" MyProg "?query=" entity "&page=" int(PBSize) * (int(pbon) - 1 ) "\" style=\"text-decoration: none; color: #00C\"><span class=\"bold\">Prev</span></a></a><span class=\"tab20\"></span>"
                else if ( RunType == "standalone" )
                    pageindex = pageindex "<a href=\"" URLPrefix "?query=" entity "&page="  int(PBSize) * (int(pbon) - 1 ) "\" style=\"text-decoration: none; color: #00C\"> <span class=\"bold\">Prev</span></a><span class=\"tab20\"></span>"
                else
                    pageindex = pageindex " error"
	    }
	    while ( ipage++ < zpage ) {
                if(ipage != pagenum) {
                    if ( RunType == "cgi" )
                        pageindex = pageindex "<a href=\"" URLPrefix "/" MyProg "?query=" entity "&page=" ipage "\" style=\"text-decoration: none; color: #00C\">" ipage "</a>"
                    else if ( RunType == "standalone" )
                        pageindex = pageindex "<a href=\"" URLPrefix "?query=" entity "&page=" ipage "\" style=\"text-decoration: none; color: #00C\" >" ipage "</a>"
                    else
                        pageindex = pageindex " error"
                } else
                    pageindex = pageindex "[" ipage "]"
                ipage != numpages ? pageindex = pageindex " " : ""
            }
            # "Next"
            if ( pbtot > 1 && pbon != pbtot ) {
                if ( RunType == "cgi" )
                    pageindex = pageindex "<span class=\"tab20\"><a href=\"" URLPrefix "/" MyProg "?query=" entity "&page=" ipage "\" style=\"text-decoration: none; color: #00C\" ><span class=\"bold\">Next</span></a></span>"
                else if ( RunType == "standalone" )
                    pageindex = pageindex "<span class=\"tab20\"><a href=\"" URLPrefix "?query=" entity "&page=" ( (int(PBSize) * int(pbon)) + 1) "\" style=\"text-decoration: none; color: #00C\" > <span class=\"bold\">Next</span></a></span>"
                else
                    pageindex = pageindex " error"
	    }
            pageindex = pageindex "</div><br>"
        }

        page = page pageindex

        i = visible = 0
        while(i < (Rows * 2) ) {
            i = i + 2
            delete G

       # identifier (name="identifier">presidentgarfiel00hinsuoft<)
            match(doc[i], "name=\"identifier\">[^<]+<", arr)
            split(arr[0],arrg,"(<|>)")
            G["identifier"] = arrg[2]
            if ( G["identifier"] == "" )
              continue #Skip if no id
            else
              visible++  

       # creator and subject (if multiple names they are surrounded by <arr .. </arr> and each enclosed by a <str></str> - somtimes multiple names are just plain text comma separated)

            ad = 0
            ac = split(doc[i], arr, "\n") # possible point of failure if XML format ever stoped having a /n after each line
            while(ad++ < ac) {
              if(arr[ad] ~ /<arr name=\"creator\">/ ) {
                gsub(/<arr name=\"creator\">/,"",arr[ad])
                gsub(/<\/arr>/,"",arr[ad])
                split(strip(arr[ad]), arrg, "<\/str>|<str>")
                G["creatorfirst"] = hightokens(strip(arrg[2]))
                G["creator"] = strip(arr[ad])
                gsub(/<\/str><str>/,"; ",G["creator"])
                gsub(/<\/str>|<str>/,"",G["creator"])
                G["creator"] = hightokens(G["creator"])
                continue
              }
              if(arr[ad] ~ /<str name=\"creator\">/ ) {
                gsub(/<str name=\"creator\">/,"",arr[ad])
                gsub(/<\/str>/,"",arr[ad])
                G["creator"] = hightokens(strip(arr[ad]))
                G["creatorfirst"] = G["creator"]
                continue
              }
              if(arr[ad] ~ /<arr name=\"subject\">/ ) {
                gsub(/<arr name=\"subject\">/,"",arr[ad])
                gsub(/<\/arr>/,"",arr[ad])
                G["subject"] = strip(arr[ad])
                gsub(/<\/str><str>/,"; ",G["subject"])
                gsub(/<\/str>|<str>/,"",G["subject"])
                G["subject"] = hightokens(G["subject"])
                continue
              }
              if(arr[ad] ~ /<str name=\"subject\">/ ) {
                gsub(/<str name=\"subject\">/,"",arr[ad])
                gsub(/<\/str>/,"",arr[ad])
                G["subject"] = hightokens(strip(arr[ad]))
                continue
              }
            }

       # description (name="description">text<)
            split(doc[i], arr, "(<str name=\"description\")")
            split(arr[2], arrg, "</str>")
            G["description"] = hightokens(substr(striphtml(substr(arrg[1], 2)), 1, 320))  # Only show first 320 characters of description field

       # downloads (name="downloads">638<)
            match(doc[i], "name=\"downloads\">[0-9]+<", arr)
            split(arr[0],arrg,"(<|>)")
            G["downloads"] = arrg[2]

       # date (name="date">1881-01-01T00:00:00Z)
            match(doc[i], "name=\"date\">[^<]+<", arr)
            split(arr[0],arrg,"(<|>)")
            split(arrg[2],arrgh,"-")
            G["date"] = arrgh[1]

       # texts (name="mediatype">texts<)
            match(doc[i], "name=\"mediatype\">[^<]+<", arr)
            split(arr[0],arrg,"(<|>)")
            G["mediatype"] = arrg[2]

       # title (name="title">Title<)
            match(doc[i], "name=\"title\">[^<]+<", arr)
            split(arr[0],arrg,"(<|>)")
            G["title"] = hightokens(arrg[2])

       # cover image
            G["image"] = "//www.archive.org/download/" G["identifier"] "/page/cover_thumb.jpg"

       # display works
            page = page "<div class=\"tab100\">"
            if ( G["mediatype"] == "texts")
                page = page sprintf("\n\n<img alt=\"texts\" class=\"imageicon\" src=\"//archive.org/images/mediatype_texts.gif\" />")
            else if ( G["mediatype"] == "image")
                page = page sprintf("\n\n<img alt=\"image\" class=\"imageicon\" src=\"//archive.org/images/mediatype_image.gif\" />")
            else if ( G["mediatype"] == "video" || G["mediatype"] == "movies")
                page = page sprintf("\n\n<img alt=\"video\" class=\"imageicon\" src=\"//archive.org/images/mediatype_video.gif\" />")
            else if ( G["mediatype"] == "audio" || G["mediatype"] == "etree")
                page = page sprintf("\n\n<img alt=\"audio\" class=\"imageicon\" src=\"//archive.org/images/mediatype_audio.gif\" />")
            else if ( G["mediatype"] == "software")
                page = page sprintf("\n\n<img alt=\"software\" class=\"imageicon\" src=\"//archive.org/images/mediatype_software.gif\" />")
            else
                page = page sprintf("\n\n<img alt=\"collection\" class=\"imageicon\" src=\"//archive.org/images/mediatype_collection.gif\" />")

            page = page sprintf("\n<img src=\"%s\" width=\"80\" height=\"55\" align=\"right\" />", G["image"])

            page = page sprintf("\n<span class=\"bold\"><a href=\"//archive.org/details/%s\" style=\"color: #00C\">%s</a></span> - %s</div>", G["identifier"], G["title"], G["creatorfirst"])

            page = page "<div class=\"tab125\">"

            G["description"] ? page = page sprintf("\n%s<br>", G["description"]) : ""
            G["date"] ? page = page sprintf("\n<span class=\"bold\">Date</span>: %s<br>", G["date"]) : ""
            G["creator"] ? page = page sprintf("\n<span class=\"bold\">Creator</span>: %s<br>", G["creator"]) : ""
            if(G["subject"]) {
                if ( RunType == "cgi" ) 
                    page = page sprintf("\n<span class=\"bold\">Keywords</span>: <a href=\"%s/%s?query=%s\" style=\"color: #00C\" >%s</a><br>", URLPrefix, MyProg, urlencode("subject:\"" G["subject"] "\""), G["subject"])
                else if ( RunType = "standalone" ) 
                    page = page sprintf("\n<span class=\"bold\">Keywords</span>: <a href=\"%s/?%s=%s\" style=\"color: #00C\" >%s</a><br>", URLPrefix, MyProg, urlencode("subject:\"" G["subject"] "\""), G["subject"])
                else
		    page = page " error"
            }
            G["downloads"] ? page = page sprintf("\n<span class=\"bold\">Downloads</span>: %s<br>", G["downloads"]) : ""

            page = page "</div>"

            i != (Rows * 2) ? page = page sprintf("\n<div class=\"tab180\"><hr></div>\n") : ""
        }
        ! visible ? page = page "<div class=\"tab100\">No results.</div>" : page = page pageindex

        return page
}

#
# Break total number of pages into blocks of PBSize (user defined) and return which block "pagenumber" belongs to. 
#   Also an option (command = "total") to return total number of blocks.
#
function pageblock(pagenumber, numberofpages, command    ,myfloat, arr, numberofblocks, i, calc) {

        myfloat = int(numberofpages) / int(PBSize)

        if ( myfloat ~ "[.]") {
            split(myfloat, arr, ".")
            numberofblocks = int(arr[1]) + 1
        } else
            numberofblocks = myfloat

	if ( command == "total") {
            if ( numberofblocks > 1) 
                return numberofblocks
            else
	        return 1
        }
	if ( command == "on") {
            while ( i++ < int(numberofblocks) ) {
                if ( int(pagenumber) <= i * int(PBSize) ) 
                    return i
            }
            return 1
        }
}

# _________________________ download a page _______________________________________________________

#
# http2var - replicate "wget -q -O- http://..." in pure gawk
#   Return the HTML page as a string. Handles redirects (!)
# Credit: Adapted from Peteris Krumins's "get_youtube_vids.awk"
#   https://code.google.com/p/lawker/source/browse/fridge/gawk/www/get_youtube_vids.awk
#
function http2var(url   ,urlhost,urlrequest, c,i,a,p,f,j,output, foO, headerS, matches, inetfile, request, loop)
{

        if ( WTA == "wget" ) {                                 # Use wget if available (ie. Unix)
                output = sys2var("wget --user-agent=\"" Agent "\" -q -O- \"" url "\"")
                return output
        }                                                      # else fallback to Awk networking (ie. Windows)

        # Assumes a URL containing standard Internet Archive syntax
        # eg: http://archive.org/advancedsearch.php?...

        split(url, a, "/")
        urlhost = a[3]                                         # eg. archive.org
        split(url, a, "[?]")
        urlrequest = "/advancedsearch.php?" a[2]               # <-- Custom lead for Internet Archive 

        inetfile = "/inet/tcp/0/" urlhost "/80"
        request = "GET " urlrequest " HTTP/1.0\r\n"            # 1.1 doesn't work (due to Transfer-Encoding: chunked ?)
        request = request "Host: " urlhost "\r\n"
        request = request "User-Agent: " Agent ")\r\n"         # <-- Custom API Agent string
        #request = request "Accept-Encoding: gzip \r\n"        # To Do
        request = request "Cache-Control: no-cache \r\n"
        request = request "\r\n\r\n"

        do {
            get_headers(inetfile, request, headerS)
            if ("Location" in headerS) {
                close(inetfile)
                if (match(headerS["Location"], /http:\/\/([^\/]+)(\/.+)/, matches)) {
                    foO["InetFile"] = "/inet/tcp/0/" matches[1] "/80"
                    foO["Host"]     = matches[1]
                    foO["Request"]  = matches[2]
                }
                else {
                    foO["InetFile"] = ""
                    foO["Host"]     = ""
                    foO["Request"]  = ""
                }
                inetfile = foO["InetFile"]
                request  = "GET " foO["Request"] " HTTP/1.0\r\n"
                request  = request "Host: " foO["Host"] "\r\n"
                request  = request "User-Agent: " Agent ")\r\n"         # <-- Custom API Agent string
                request  = request "Cache-Control: no-cache \r\n"
                request  = request "\r\n\r\n"
                if (inetfile == "") {
                    print "Failed 1 (" url "), got caught in Location loop!" > "/dev/stderr"
                    return -1
                }
            }
            loop++
        } while (("Location" in headerS) && loop < 5)
        if (loop == 5) {
            print "Failed 2 (" url "), got caught in Location loop!" > "/dev/stderr"
            return -1
        }

        while ((inetfile |& getline) > 0) {
            j++
            output[j] = $0
        }
        close(inetfile)
        if(len2(output) == 0)
            return -1
        else
            return join(output, 1, j, "\n")
}
function get_headers(Inet, Request, headerS	,save_rs) {

        delete headerS
        save_rs=RS

        print Request |& Inet

        # get the http status response
        if (Inet |& getline > 0) {
            headerS["_status"] = $2
        }
        else {
            print "Failed reading from the net in get_headers()"
            return
        }

        RS="\r\n"
        while ((Inet |& getline) > 0) {
            if (match($0, /([^:]+): (.+)/, Matches)) {
                headerS[Matches[1]] = Matches[2]
            }
            else { break }
        }
        RS=save_rs
}

# _________________________ highlight search tokens _______________________________________________________

#
# highlight search tokens 
#
function hightokens(str		,c,arr,i,t,out,build,work) {

	c = split(str, arr, " ")
	while(i < c) {
	  i++
          out = ""
          work = arr[i]
          gsub("([,]|^[(]|[)]$|[)][,]$|\"|[.]$|\"[.]$|'[.]$|`[.]$|;|[:]|^'|'$|^`|`$|'s$|'s[.]$)","",work)               # <-- Customize search token exceptions
	  for ( t in Tokens ) {
            gsub("([,]|^[(]|[)]$|[)][,]$|\"|[.]$|\"[.]$|'[.]$|`[.]$|;|[:]|^'|'$|^`|`$|'s$|'s[.]$)","",t)
	    if ( tolower(t) == tolower(work) ) {
              out = "<span class=\"high\">" arr[i] "</span>"
              break
            }
	  }
          out == "" ? out = arr[i] : ""
          i == 1 ? build = out : build = build " " out
        }
        return build
}
function init_searchtokens(iaquery	,arr, arrg, i, k, c) {

	gsub("\"","",iaquery)
	gsub("+"," ",iaquery)
	split(iaquery, arr, "+") 
        k = join(arr, 1, len2(arr), " ")        
        c = split(k, arrg, " ")
        while ( i++ < c) {
          if ( arrg[i] !~ "(AND|OR|:)" ) {           # <-- Customize tokens to ignore eg. logic statements, etc..
            gsub(/[,]+$/,"",arrg[i])
            gsub(/[)]+$/,"",arrg[i])
            gsub(/^[(]+/,"",arrg[i])
            Tokens[arrg[i]] = arrg[i]
          }
        }
}

# _________________________ utilities _______________________________________________________

#
# Run a system command and store result in a variable
#   eg. googlepage = sys2var("wget -q -O- http://google.com")
# Supports pipes inside command string. Stderr is sent to null.
# If command fails (errno) return null 
#   Credit: Stephen Balbach
#
function sys2var(command        ,fish, scale, ship) {

         command = command " 2>/dev/null"
         while ( (command | getline fish) > 0 ) {
             if ( ++scale == 1 )
                 ship = fish
             else
                 ship = ship "\n" fish
         }
         close(command)
         return ship
}

#
# Merge an array of strings into a single string. Array indice are numbers.
#  Credit: GNU awk manual
#
function join(array, start, end, sep,    result, i)
{
	result = array[start]
	for (i = start + 1; i <= end; i++)
	    result = result sep array[i]
	return result
}

#
# url-encode a string
#  Credit: Rosetta Stone May 2015
#
function urlencode(str,  c, len, res, i, ord) {

	for (i = 0; i <= 255; i++)
		ord[sprintf("%c", i)] = i
 	len = length(str)
	res = ""
	for (i = 1; i <= len; i++) {
		c = substr(str, i, 1);
		if (c ~ /[0-9A-Za-z]/)
			res = res c
		else
			res = res "%" sprintf("%02X", ord[c])
	}
	return res
}

#
# Length of an array. Portable function for older versions of gawk
#
function len2(array, i) {
	i = 1
	while (i in array) {
            i++
        }
        return i - 1
}

#
# strip HTML (method has general limitations but OK for this app)
#
function striphtml(s) {
	gsub (/<[^>][^>]*>/, "", s)
	return s
}

#
# Strip leading/trailing whitespace
#
function strip(str)
{
        gsub(/^[[:space:]]+|[[:space:]]+$/,"",str)
        return str
}


