Internet Archive Classic Search
===============================

IACS emulates the search interface used on the old or "classic" IA website.
Search results have page numbers, not infinite scroll. Search results are otherwise
identical.

Demo: http://balbach.net/cgi-bin/iacs.awk

Installation
=============

The script is GNU awk and will run on any system that has gawk including Windows,
most unix'es and Mac. Linux systems have it by default, Windows can download a 
single binary .exe for free.

The script runs with two methods:

* 1) As a regular CGI script on an existing websever.
* 2) The script includes a built-in webserver and will run on any computer as
a standalone webserver application.

Follow the configuration instructions at the top of the script.

For Windows users:

* Download GNU Awk for Windows from http://www.klabaster.com/freeware.htm#dl
* Place awk.exe and iacs.awk in the same directory.
* Edit iacs.awk and follow configuration instructions. 
* Set RunType = "standalone", MyHost = "localhost", MyPort = "8080"
* From a command prompt start the server: awk.exe -f iacs.awk
* In a browser connect to the server: http://localhost:8080/

Information
=============

The "standalone" webserver built into the script is not recommended to run as an 
open service on the Internet. The webserver is adequete for light usage but will 
bog down with multiple users. For security the standalone webserver should be behind 
a firewall or NAT router and not openly accessible to the Internet. 

The "cgi" mode is limited only by the performance and security of your webserver.
When running in "cgi" mode the builtin webserver is not enabled or running.

Credits and License
===================
Internet Archive Classic Search 

by Stephen Balbach

iacs2015@nym.hush.com

https://bitbucket.org/stbalbach/InternetArchiveClassicSearch

MIT License
